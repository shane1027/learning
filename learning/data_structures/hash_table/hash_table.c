#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#define DEBUG false

#define NUMENTRIES 1200
#define SIZE 16384
#define ENTRY 25

int hash(char*);
char** createTable(int);
void deleteTable(char**, int);
bool insertItem(char**, char*, char*);
char* lookupItem(char**, char*);
void removeItem(char**, char*);
void printTable(char**, char[][2][ENTRY], int);
char*** genPeople(int);
void deletePeople(char***, int);
void staticTest(char **);
void dynamicTest(char **);
static char *rand_string(char *, size_t);

int main(void) {
	// seed rand
	srand(time(NULL));	

    // allocate array for hash table
    char **table = createTable(SIZE);

    // test with small amount of statically allocated data
    staticTest(table);

    // test with large amount of dynamically allocated data
    dynamicTest(table);

    // deallocate table
    deleteTable(table, SIZE);

    return 0;
}

void dynamicTest(char **table) {
    // generate random input data
    char ***randos = genPeople(NUMENTRIES);

	// collision counter
	int collisions = 0;

    // add each person to the hash table
    for (int i = 0; i < NUMENTRIES; i++) {
        if (insertItem(table, randos[i][0], randos[i][1])) {
			collisions++;
		}
        if (DEBUG)
	        printf("Collisions: %d\n", collisions);
    }

	printf("Collisions: %d\n", collisions);

    // TODO
    // test lookup from table
    // printTable(table, people, peopleCount);

    // test removal from table
    // removeItem(table, "Shane");
    // printTable(table, people, peopleCount);
    
    // free data
    deletePeople(randos, NUMENTRIES);
}

void staticTest(char **table) {
    // add string mapping to table
    char people[][2][ENTRY] = {{"Shane", "Chicago"}, {"Michael", "Arizona"},
                            {"Shu", "Sri Lanky"}, {"Forrest", "Alabama"}};

    int peopleCount = sizeof(people) / sizeof(people[0]);

    // add each person to the hash table
    for (int i = 0; i < peopleCount; i++) {
        insertItem(table, people[i][0], people[i][1]);
    }

    // test lookup from table
    printTable(table, people, peopleCount);

    // test removal from table
    removeItem(table, "Shane");
    printTable(table, people, peopleCount);
    
    // remove all static items to reset table
    for (int i = 0; i < peopleCount; i++) {
        removeItem(table, people[i][0]);
    }
}

char ***genPeople(int num) {
    char ***people = (char ***)malloc(num * sizeof(char **));
    if (DEBUG)
        printf("allocated people[]\n");
    for (int j = 0; j < num; j++) {
        people[j] = (char **)malloc(2 * sizeof(char *));
        if (DEBUG)
            printf("allocated people[%d][]\n", j);
        people[j][0] = (char *)malloc(ENTRY * sizeof(char));
        people[j][1] = (char *)malloc(ENTRY * sizeof(char));
    }
    for (int i = 0; i < num; i++) {
		rand_string(people[i][0], ENTRY/2);
        strcpy(people[i][1], "PLACE");
        if (DEBUG)
            printf("%s\t%s\n", people[i][0], people[i][1]);
    }
    return people;
}

static char *rand_string(char *str, size_t size)
{
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (size) {
        --size;
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

void deletePeople(char ***people, int peopleCount) {
    for (int i = 0; i < peopleCount; i++) {
        for (int j = 0; j < 2; j++) {
            free(people[i][j]);
        }
        free(people[i]);
    }
    free(people);
}

void printTable(char **table, char people[][2][ENTRY], int peopleCount) {
    for (int i = 0; i < peopleCount; i++) {
        char *name = people[i][0];
        char *place = lookupItem(table, name);
        if (place != NULL) {
            printf("%s hails from %s\n", name, place);
        } else {
            printf("%s is not in table!\n", name);
        }
    }
    putchar('\n');
}

char** createTable(int size) {
    char **table = malloc(sizeof(char*) * size);
    for (int i = 0; i < size; i++) {
        table[i] = NULL;
    }
    return table;
}

void deleteTable(char **table, int size) {
    printf("Freeing table of size %d\n", size);
    int count = 0;
    for (int i = 0; i < size; i++) {
        if (table[i]) {
            free(table[i]);
            count++;
        }
    }
    if (DEBUG)
        printf("Freed %d blocks\n", count);
    free(table);
}

bool insertItem(char **table, char *name, char *place) {
    int key = hash(name);
    if (DEBUG)
        printf("Inserting place %s for name %s at key %d\n", place, name, key);
    if (table[key]) {
        if (DEBUG)
            printf("Collision!\nIndex %d already contains %s\n", key, table[key]);
        // implement collision handling here.. easiest is open addressings,
        // simply incrementing the key until there is an unused val...
        // alternatively, could build a linked-list or bst w/ root @ this spot
		return true;
    } else {
        table[key] = malloc(ENTRY * sizeof(char));
        strncpy(table[key], place, (size_t)ENTRY);
	    return false;
    }
}

char* lookupItem(char **table, char *name) {
    int key = hash(name);
    return table[key];
}

void removeItem(char **table, char *name) {
    int key = hash(name);
    free(table[key]);
    table[key] = NULL;
}

// djb2 hashing function for strings... better than murmurhash?
int hash(char *input) {
    char *iter = input;
    unsigned long hash = 5381;
    int c;

    while (c = *iter++)
        hash = ((hash << 5) + hash) + c;
    hash = hash % SIZE;

    if (DEBUG)
        printf("input: %s\nindex: %d\n", input, hash);
    return hash; 
}
