#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct node {
    int val;
    struct node* left;
    struct node* right;
};

struct node* genTree(int*, int);
struct node* newNode(int);
struct node* insertNode(struct node*, int);
bool searchTree(struct node*, int);
void printTree(struct node*);
void freeTree(struct node*);

int main(void) {
    
    int vals[] = {7, 3, 1, 2, 12, 8, 9, 14};
    int size = sizeof(vals) / sizeof(vals[0]);
    struct node* bst = genTree(vals, size);

    printf("\nresulting tree in-order:\n");
    printTree(bst);
    printf("\n\n");

    for (int i = 0; i < 20; i++) {
        printf("Searching for %d in tree...\t", i);
        printf("%s\n", searchTree(bst, i) ? "Found." : "Not Found.");
    }

    freeTree(bst);
    return 0;
}

struct node* newNode(int val) {
    struct node* node = malloc(sizeof(struct node));
    node->val = val;
    node->left = NULL;
    node->right = NULL;
    return node;
}

// return root pointing to modified tree
struct node* insertNode(struct node* root, int val) {
    if (!root) {
        return newNode(val);
    }
    else if (val > root->val) {
        root->right = insertNode(root->right, val);
    } else if (val < root-> val) {
        root->left = insertNode(root->left, val);
    }
    return root;
}

struct node* genTree(int *vals, int size) {
    if (!vals)
        return NULL;

    struct node* root = NULL;
    for (int i = 0; i < size; i++) {
        printf("inserting %d...\n", vals[i]);
        root = insertNode(root, vals[i]);
    }
    
    return root;
}

void printTree(struct node* tree) {
    if (tree) {
        printTree(tree->left);
        printf("%d\t", tree->val);
        printTree(tree->right);
    } else {
        return;
    }
}

bool searchTree(struct node* tree, int val) {
    if (!tree)
        return false;
    if (tree->val == val) {
        return true;
    } else if (tree->val < val) {
        return searchTree(tree->right, val);
    } else if (tree->val > val) {
        return searchTree(tree->left, val);
    }
}

void freeTree(struct node* root) {
    if (!root) {
        return;
    } else {
        freeTree(root->left);
        freeTree(root->right);
        free(root);
    }
}
