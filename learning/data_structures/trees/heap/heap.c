#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define DEBUG FALSE

// probably don't need this...
typedef struct Node {
    int val;
    struct Node* left;
    struct Node* right;
} Node;

void swap(int *, int*);
int *heapify(int *, int*);
void insert(int *, int);
int popMax(int *);
void print_heap(int *);

int main(void) {
    int vals[] = { 1, 8, 3, 5, 6, 3, 5, 7, 8, 13, 15, 1, 4, 18, 2 };
    int heapSize = sizeof(vals) / sizeof(vals[0]);

    printf("Unsorted vals:\n");
    for (int i = 0; i < heapSize; i++) {
        printf("%d\t", vals[i]);
    }

    int *heap = heapify(vals, vals+heapSize-1);

    printf("\n\nResulting heap:\n\n");
    print_heap(heap);

    printf("\nSorted vals:\n");
    for (int i = 0; i < heapSize; i++) {
        printf("%d\t",popMax(heap));
    }
    printf("\n");

    free(heap);
    return 0;
}

void swap(int *dest, int *src) {
    int tmp = *dest;
    *dest = *src;
    *src = tmp;
}

int popMax(int *heap) {
    // store max val in tmp
    int max = heap[1];
    int heapSize = heap[0];

    // place last val into max
    heap[1] = heap[heapSize--];
    heap[0] = heapSize;
  
  
    // if multiple nodes, refactor... otherwise return one node. 
    if (heap[0]) {

      if (DEBUG) {
          printf("heapifying the following:\n");
          print_heap(heap);
      }
  
      int child = 0;
      // top down comparison to settle node in correct pos
      for (int curr = 1; 2*curr < heapSize; curr = child) {
  
          // compare and swap with the larger of two children if necessary
          child = 2*curr;
  
          // check to see if rchild exists and is larger
          if (child != heapSize && heap[child+1] > heap[child]) {
              child++;
          }
  
          // check to see if the larger of the two children is larger than curr
          if (heap[curr] < heap[child]) {
              // child is larger, need to swap
              swap(&heap[curr], &heap[child]);
              curr = child;
              if (DEBUG) {
                  printf("Swapped with child:\n");
                  print_heap(heap);
              }
          } else {
              break;
          }
      }
    }
        
    // reduce heap size
    heap = realloc(heap, sizeof(int) * (heapSize + 1));

    // return max val
    return max;
}

void insert(int *heap, int val) {
    // allocate space for new value
    int heapSize = heap[0];
    heap = realloc(heap, sizeof(int) * (++heapSize + 1));
    heap[0] = heapSize;

    // start by inserting at the next available slot
    int curr = heapSize;
    heap[curr] = val;

    if (DEBUG) {
        printf("inserting value %d\t size now %d\n", val, heapSize);
        printf("after insertion:\n");
        print_heap(heap);
    }

    // bottom up comparison, switching with parent if necessary
    for (int parent = (curr / 2); parent > 0; parent /= 2) {
        if (heap[parent] < heap[curr]) {
            // parent is smaller, need to swap
            swap(&heap[parent], &heap[curr]);
            curr = parent;
            if (DEBUG) {
                printf("Swapped with parent:\n");
                print_heap(heap);
            }
        } else {
            break;
        }
    }
}

int *heapify(int *start, int *end) {
    if (!*start) {
        return NULL;
    }
    int *vals = start;
    int heapSize = 1;

    // add 1 to size to account for heap size storage
    int *heap = malloc(sizeof(int) * (heapSize + 1));
    heap[0] = heapSize;
    heap[1] = *vals++;
    // ok, now we have a heap!

    if (DEBUG) {
        printf("Starting heap:\n");
        print_heap(heap);
    }

    // for each remaining item, add it to appropriate spot in heap
    while (vals <= end) {
        insert(heap, *vals++);        
    }

    return heap;
}

void print_heap(int *heap) {
    // height of heap is floor(log2(heapSize) + 1)
    int size = heap[0];
    int height = size ? floor(log2(size) + 1) : 0;
    printf("Heap Size: %d\n", size);
    printf("Heap Height: %d\n", height);

    int counter = 1;
    for (int i = 0; i < height; i++) {
        printf("Level %d:\t", i);
        for (int j = pow(2,i); j < 2*pow(2,i); j++) {
            printf("%d\t", heap[j]);
            if (counter++ == size)
                break;
        }
        printf("\n");
    }
}
