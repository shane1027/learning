# Data Structures

Useful abstract methods of storing data for various efficiencies.


### [Hash Tables](https://www.wikiwand.com/en/Hash_table)

Associative array that maps keys to values.  Average lookup cost is
independent of number of items stored in the structure, as are insertion and
deletion.

Hashing functions should spread key values over a large range.  Here are some
discussions on [typical hash functions](http://www.cse.yorku.ca/~oz/hash.html)
and [collision
handling](https://preshing.com/20110504/hash-collision-probabilities/).

### Linked Lists

Singly and doubly linked... you got it.  Items in non-sequential blocks of data
related via pointers.


### TREE BOIS

#### Binary Tree

Binary Tree is a special data structure used for data storage purposes. A binary
tree has a special condition that each node can have a maximum of two children.
A binary tree has the benefits of both an ordered array and a linked list as
search is as quick as in a sorted array and insertion or deletion operation are
as fast as in linked list.

#### [Self-Balancing Binary Search Tree](https://www.wikiwand.com/en/Self-balancing_binary_search_tree)

> ...self-balancing BSTs have a number of advantages and
 disadvantages over their main competitor, hash tables. One advantage of
 self-balancing BSTs is that they allow fast (indeed, asymptotically optimal)
 enumeration of the items in key order, which hash tables do not provide. One
 disadvantage is that their lookup algorithms get more complicated when there
 may be multiple items with the same key. Self-balancing BSTs have better
 worst-case lookup performance than hash tables (O(log n) compared to O(n)),
 but have worse average-case performance (O(log n) compared to O(1)). 

#### [AVL Tree](https://www.geeksforgeeks.org/avl-tree-set-1-insertion/)

Type of self-balancing binary search tree that prioritizes balance over cost of
frequent rotations on insertion / deletion.

Should implement a version of the code in the link above for practice.

Check [here](https://www.geeksforgeeks.org/avl-tree-set-2-deletion/) for
further info on deletion.

#### Red-Black Tree

Type of self-balancing binary search tree that keeps balance within a scale
factor (usually 1.44) of being perfectly balanced, and prioritizes low overhead
for frequent insertion / deletion applications.

#### Heap

Heap is a complete binary tree... has two children per node, can be traversed 
based on a condition with a binary outcome.

Two types of heaps:

- Max Heap has the greatest key present at the root node.

- Min Heap has the smallest key present at the root node.

These conditions are recursively true for all children.


As a reminder, a complete tree is defined as a tree meeting the following:

1. Levels are completely filled except possibly the last level
2. The last level has all keys as left as possible

This property allows them to be stored in an array.


*Frequent use cases for Heaps:*

- Heapsort -> great in-place sorting algo w/ no quadratic worst-case

- Selection Algos -> access min / max in constant time, kth element or median
  element in sub-linear time

- Shortest Path Graph Alogs -> spanning a graph

- Priority Queue -> such as process dependency for a scheduler

- K-way merge -> Given k sets of pre-sorted data, merge into single sorted
  array... add first val from all sets to a min-heap, pop min into final sorted
  array, and add next val from the set the min was popped from to the heap at
  root... sift down (heapify) and repeat

- Order Statistics -> finding the kth largest or smallest element (heapify
  data, pop k times)


Great discussion on O(n) heap construction and O(nlogn) heap sort
[here](https://stackoverflow.com/questions/9755721/how-can-building-a-heap-be-on-time-complexity).

```C
int *heapify(int *start, int *end) {
    if (!*start) {
        return NULL;
    }
    int *vals = start;
    int heapSize = 1;

    // add 1 to size to account for heap size storage
    int *heap = malloc(sizeof(int) * (heapSize + 1));
    heap[0] = heapSize;
    heap[1] = *vals++;
    // ok, now we have a heap!

    // for each remaining item, add it to appropriate spot in heap
    while (vals <= end) {
        insert(heap, *vals++);        
    }

    return heap;
}

void swap(int *dest, int *src) {
    int tmp = *dest;
    *dest = *src;
    *src = tmp;
}

void insert(int *heap, int val) {
    // allocate space for new value
    int heapSize = heap[0];
    heap = realloc(heap, sizeof(int) * (++heapSize + 1));
    heap[0] = heapSize;

    // start by inserting at the next available slot
    int curr = heapSize;
    heap[curr] = val;

    // compare with parent, switching if necessary
    for (int parent = (curr / 2); parent > 0; parent /= 2) {
        if (heap[parent] < heap[curr]) {
            // parent is smaller, need to swap
            swap(&heap[parent], &heap[curr]);
            curr = parent;
        } else {
            break;
        }
    }
}

int popMax(int *heap) {
    // store max val in tmp
    int max = heap[1];
    int heapSize = heap[0];

    // place last val into max
    heap[1] = heap[heapSize--];
    heap[0] = heapSize;
  
  
    // if multiple nodes, refactor... otherwise return one node. 
    if (heap[0]) {
  
      int child = 0;
      // top down comparison to settle node in correct pos, starting @ root
      for (int curr = 1; 2*curr < heapSize; curr = child) {
  
          // compare and swap with the larger of two children if necessary
          child = 2*curr;
  
          // check to see if rchild exists, and is larger
          if (child != heapSize && heap[child+1] > heap[child]) {
              child++;
          }
  
          // check to see if the larger of the two children is larger than curr
          if (heap[curr] < heap[child]) {
              // child is larger, need to swap
              swap(&heap[curr], &heap[child]);
              curr = child;
          } else {
              break;
          }
      }
    }
        
    // reduce heap size
    heap = realloc(heap, sizeof(int) * (heapSize + 1));

    // return max val
    return max;
}

```

Good info on heaps
[here](https://www.thelearningpoint.net/computer-science/data-structures-heaps-with-c-program-source-code)
and
[here](https://www.tutorialspoint.com/data_structures_algorithms/heap_data_structure.htm).

Need to learn how to implement a recursive binary heap, such as
[here](https://www.sanfoundry.com/c-program-to-implement-binary-heap/).
