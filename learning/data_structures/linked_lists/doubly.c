#include <stdio.h>
#include "doubly.h"

int main(void) {
    struct node* dbl_list;
    dbl_list = populate_list(10);

    printf("Generated Linked List:\n\n");
    print_nodes(dbl_list);

    printf("\nReversed In-Place:\n\n");
    dbl_list = reverse_list(dbl_list);
    print_nodes(dbl_list);

    printf("\nRandom Node Selection:\n\n");
    struct node* randNode = pick_random(dbl_list);
    print_node(randNode);

    printf("\nFind and Replace (find 3, replace w/ 5):\n\n");
    find_and_replace(dbl_list, 3, 5);
    print_nodes(dbl_list);

    _free(dbl_list);

    return 0;
}
