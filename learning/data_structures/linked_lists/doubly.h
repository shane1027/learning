// doubly.h

struct node {
    int val;
    struct node* prev;
    struct node* next;
};

void print_nodes(struct node*);
struct node* populate_list(int);
struct node* reverse_list(struct node*);
struct node* pick_random(struct node*);
void find_and_replace(struct node*, int find, int repl);
void print_node(struct node*);
void _free(struct node*);
