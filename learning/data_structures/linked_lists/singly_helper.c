#include <stdio.h>
#include <stdlib.h>
#include "singly.h"

struct node* genList(int listSize) {
    // zero node result
    if (!listSize) {
        return NULL;
    }

    // init first node
    struct node* curr = createNode(random() % 100);

    // save pointer to root node
    struct node* root = curr;

    // build remaining nodes
    for (int i = 1; i < listSize; i++) {
        curr->next = createNode(random() % 100);
        curr = curr->next;
    }

    return root;
}

struct node* createNode(int val) {
    struct node* newnode = (struct node*)malloc(sizeof(struct node));
    newnode->val = val;
    newnode->next = NULL;
    return newnode;
}

struct node* reverse_list(struct node* root, struct node* prev) {
    // handle end of the list
    if (!root->next) {
        root->next = prev;
        return root;
    }
    struct node* tmp = root->next;
    root->next = prev;
    return reverse_list(tmp, root);
}

void print_nodes(struct node* root) {
    struct node* nodeiter = root;
    while (nodeiter) {
        if (nodeiter != root)
            printf(" -> ");
        printf("{ val: %d }", nodeiter->val);
        nodeiter = nodeiter->next;
    }
    printf("\n");
}

void _free(struct node* root) {
    if (root->next) {
        _free(root->next);
    }
    free(root);
}
