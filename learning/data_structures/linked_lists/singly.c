#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "singly.h"

int main(void) {

    srand(time(NULL));

    struct node* list = genList(10);
    print_nodes(list);

    list = reverse_list(list, NULL);
    print_nodes(list);

    _free(list);

    return 0;

}

