#include <stdio.h>
#include <stdlib.h>
#include "doubly.h"

#define true 1
#define false 0
#define DEBUG false

struct node* populate_list(int listSize) {
    // no nodes
    if (!listSize) {
        return NULL;
    }

    // create root node
    struct node* root;

    // create first node
    struct node* curr = (struct node*)malloc(sizeof(struct node));
    curr->val = random() % 10;
    curr->next = NULL;
    curr->prev = NULL;
    root = curr;

    for (int i = 1; i < listSize; i++) {
       // allocate remaining nodes
       curr->next = (struct node*)malloc(sizeof(struct node));
       curr->next->val = random() % 10;
       curr->next->prev = curr;
       curr->next->next = NULL;
       curr = curr->next;
    }

    return root;
}

void print_nodes(struct node* root) {
    struct node* nodeiter = root;
    while (nodeiter) {
        if (nodeiter != root)
            printf(" <-> ");
        printf("{ val: %d }", nodeiter->val);
        nodeiter = nodeiter->next;
    }
    printf("\n");
}

struct node* reverse_list(struct node* root) {
    struct node* newroot = root;
    struct node* curr = root;
    while (curr) {
        struct node* tmp = curr->next;
        curr->next = curr->prev;
        curr->prev = tmp;
        newroot = curr;
        curr = tmp;
    }
    return newroot;
}

struct node* pick_random(struct node* root) {
    // reservoir sampling to ensure equal node selection chance
    // without prior knowledge of stream length
    struct node* randNode = root;
    struct node* curr = root->next;
    float i = 2;
    while (curr) {
        int chance = (i-1) / i * 100;
        int rand = random() % 100;
        if (DEBUG) {
            printf("Chances of selecting node %03.0f: %d\n", i, chance);
            printf("Random outcome: %d\n", rand);
        }
        if (chance <= rand) {
            if (DEBUG) {
                printf("New node selected!\n");
                print_node(randNode);
            }
            randNode = curr;
        }
        curr = curr->next;
        i += 1;
    }
    return randNode;
}

void find_and_replace(struct node* root, int find, int repl) {
    struct node* curr = root;
    while (curr) {
        if (curr->val == find) {
            curr->val = repl;
        }
        curr = curr->next;
    }
}

void print_node(struct node* node) {
    if (node) {
        int nextVal = -1;
        int prevVal = -1;
        
        if (node->next) {
            nextVal = node->next->val;
        }

        if (node->prev) {
            prevVal = node->prev->val;
        }

        printf("{ val: %d\tprevVal: %d\tnextVal: %d }\n", node->val, prevVal, nextVal);
    }
}

void _free(struct node* root) {
    if (root->next) {
        _free(root->next);
    }
    free(root);
}
