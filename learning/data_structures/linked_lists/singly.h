// singly.h

struct node {
    int val;
    struct node *next;
};

struct node* createNode(int val);
struct node* genList(int listSize);
struct node* reverse_list(struct node* root, struct node* prev);
void print_nodes(struct node* root);
void _free(struct node* root);
