# Atomicity

### [The quality of being executed in a single instruction](https://stackoverflow.com/questions/36955884/what-are-atomic-types-in-the-c-language).

This means objects made of atomic types are the *only* objects [free from](https://en.cppreference.com/w/c/language/atomic) [data
  race conditions](https://en.cppreference.com/w/c/language/memory_model).


The demo snippet below shows what happens when thousands of threads attempt
to access the same variable...

Inevitably, when issued on a non-atomic data type, which takes more than one
operation to read or write (in this case, write via incrementing), the counter
undershoots the target value.  Some of the increments were issued to the
variable while the variable had already been modified but not yet saved by
another thread, leading to multiple threads performing the increment on the
same value.

With an atomic type, each thread attempts to increment the value, and is able
to do so successfully since there is at least one operation cycle in between each
thread spawning.

This is using GCC C11.

