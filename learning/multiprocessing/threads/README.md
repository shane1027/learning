Useful links:

http://www.csc.villanova.edu/~mdamian/threads/posixthreads.html

http://www.personal.kent.edu/%7Ermuhamma/OpSystems/Myos/threads.htm

https://www.geeksforgeeks.org/multithreading-c-2/


Benefits of threads over processes:

- Creation is much faster, lower overhead

- Context switching is much faster (thanks to being able to keep the TLB -
  Translation Lookaside Buffer - full, mapping virtual to physical memory more
  quickly.... and not having to replace an entire process' data, code, state,
  memory management info, open files and system resources, scheduling info)

- Context switching are fast when working with threads. The reason is that we
only have to save and/or restore PC, SP and registers. 

- All threads share the data segment of a process (global and static vars)

- Code, Data and Files are all shared by threads... each has it's own registers
  and stack.

- threads share a common address space vs. forking a process where all data and
  variables are cloned.

- inter-thread communication is much simpler because threads share the same
  address space



Need to play with threading, synchronization, mutexes and semaphores:


## Implement code from the following:

https://www.geeksforgeeks.org/mutex-lock-for-linux-thread-synchronization/

https://stackoverflow.com/questions/15182328/semaphore-implementation

https://www.embedded.com/enumerations-are-integers-except-when-theyre-not/

https://www.geeksforgeeks.org/using-fork-produce-1-parent-3-child-processes/

https://www.geeksforgeeks.org/tcp-server-client-implementation-in-c/

https://www.geeksforgeeks.org/socket-programming-cc/

https://www.geeksforgeeks.org/use-posix-semaphores-c/

http://timmurphy.org/2010/05/04/pthreads-in-c-a-minimal-working-example/


## Common algorithms explained:

http://www.personal.kent.edu/%7Ermuhamma/Algorithms/algorithm.html
