#include <stdio.h>

#define TRUE 1
#define FALSE 0
#define DEBUG FALSE

void insert(int[], int, int);
void sort(int[], int);
void printArray(int[], int);

int main(void) {
    
    int array[] = { 5, 4, 6, 9, 2, 1, 43, 2 };
    int arraySize = sizeof(array) / sizeof(int);

    printf("unsorted:\n");
    printArray(array, arraySize);
    sort(array, arraySize);
    printf("\nsorted:\n");
    printArray(array, arraySize);

    return 0;
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d\t", array[i]);
    }
    printf("\n");
}

void sort(int array[], int arraySize) {
    if (!arraySize) {
        return;
    }
    for (int i = 1; i < arraySize; i++) {
        int dest = 0;
        for (int j = i - 1; j >= 0; j--) {
            if (array[i] >= array[j]) {
                dest = j+1;
                break;
            }
        }
        insert(array, i, dest);
    }
}

void insert(int array[], int source, int destination) {
    int tmp1 = array[destination];
    array[destination] = array[source];

    for (int i = destination + 1; i <= source; i++) {
        int tmp2 = array[i];
        array[i] = tmp1;
        tmp1 = tmp2;
    }
}
