#include <stdio.h>

#define TRUE 1
#define FALSE 0
#define DEBUG FALSE

void swap(int[], int, int);
void sort(int[], int);
void printArray(int[], int);

int main(void) {
    
    int array[] = { 5, 4, 6, 9, 2, 1, 43, 2 };
    int arraySize = sizeof(array) / sizeof(int);

    printf("unsorted:\n");
    printArray(array, arraySize);
    sort(array, arraySize);
    printf("\nsorted:\n");
    printArray(array, arraySize);

    return 0;
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d\t", array[i]);
    }
    printf("\n");
}

void sort(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        int smallest = i;
        for (int j = i; j < arraySize; j++) {
            if (array[j] < array[smallest]) {
                smallest = j;
            }
        }
        if (DEBUG) {
            printf("Current index: %d\n", i);
            printf("Smallest: in remaining set: %d at index %d\n", array[smallest],
                    smallest);
            printArray(array, arraySize);
        }
        swap(array, i, smallest);
    }
}

void swap(int array[], int i1, int i2) {
    int tmp = array[i1];
    array[i1] = array[i2];
    array[i2] = tmp;
}
