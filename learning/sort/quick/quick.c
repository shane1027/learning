#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0
#define DEBUG TRUE

int *partition(int*, int*);
void sort(int*, int*);
void printArray(int[], int);
void swap(int *, int *);

int main(void) {
    
    int array[] = { 5, 4, 6, 9, 2, 1, 43, 2 };
    int arraySize = sizeof(array) / sizeof(int);

    printf("unsorted:\n");
    printArray(array, arraySize);
    sort(array, array+arraySize-1);
    printf("\nsorted:\n");
    printArray(array, arraySize);

    return 0;
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d\t", array[i]);
    }
    printf("\n");
}

void sort(int *start, int *end) {
    if (start >= end) {
        return;
    }
    if (DEBUG) {
        printf("calling partition on array:\n");
        printArray(start, (end - start) + 1);
        printf("\n");
    }
    int *offset = partition(start, end);
    if (DEBUG) {
        printf("offset value: %d\n", *offset);
        printf("array after partitioning:\n");
        printArray(start, (end - start) + 1);
        printf("\n");
    }
    sort(start, offset-1);
    sort(offset+1, end);
}

int *partition(int *start, int *end) {
    int *slow = start;
    int *fast = start;

    while (fast < end) {
        if (*fast <= *end) {
            swap(slow++, fast);
        }
        fast++;
    }

    swap(slow, end);
    return slow;
}

void swap(int *dest, int *src) {
    int tmp = *dest;
    *dest = *src;
    *src = tmp;
}
