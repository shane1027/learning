#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define DEBUG TRUE

#define ARRAYSIZE 8

int *heapify(int *, int*);
void heapSort(int *, int);
int popMin(int *);
void swap(int *, int*);
void insert(int *, int);
void print_heap(int *);


int main(void) {
    int vals[] = { 1, 8, 3, 5, 6, 3, 5, 7 };

    printf("Unsorted vals:\n");
    for (int i = 0; i < ARRAYSIZE; i++) {
        printf("%d\t", vals[i]);
    }
    printf("\n");

    heapSort(vals, ARRAYSIZE);

    printf("Sorted vals:\n");
    for (int i = 0; i < ARRAYSIZE; i++) {
        printf("%d\t", vals[i]);
    }
    printf("\n");

    return 0;
}

void swap(int *dest, int *src) {
    int tmp = *dest;
    *dest = *src;
    *src = tmp;
}

int popMin(int *heap) {
    // extract size and min
    int heapSize = heap[0];
    int min = heap[1];

    // move 'last' node into root and resize heap
    heap[1] = heap[heapSize];
    heap[0] = --heapSize;
    heap = realloc(heap, sizeof(int) * (heapSize + 1));

    // okay, now find out where new 'root' really belongs..
    int curr = 1;
    for (int child = 2*curr; child <= heapSize; child = 2*curr) {
        // check to see which child is smaller... use that one for comparisons
        if (child != heapSize && heap[child+1] < heap[child]) {
            child++;
        }
        if (heap[child] < heap[curr]) {
            swap(&heap[curr], &heap[child]);
            curr = child;
        } else {
            break;
        }
    }

    return min;
}

int *heapify(int *start, int *end) {
    if (!*start) {
        return NULL;
    }

    int *vals = start;
    int heapSize = 1;

    int *heap = malloc(sizeof(int) * (heapSize + 1));
    heap[0] = heapSize;
    heap[1] = *vals++;
    printf("starting with heap: \n");
    print_heap(heap);

    while (vals <= end) {
        insert(heap, *vals++);
        print_heap(heap);
    }

    return heap;
}

void insert(int *heap, int val) {
    // increase the heap size
    printf("inserting %d\n", val);
    int heapSize = heap[0];
    heap[0] = ++heapSize;
    printf("heapSize now %d\n", heap[0]);

    // realloc additional memory for heap
    heap = realloc(heap, sizeof(int) * (heapSize + 1));

    // start by inserting the value in the next available node, according to
    // tree complete property (fill all nodes on a given level, then fill all
    // nodes from left to right)  ((since we are in an array, appending makes
    // the value the last one in the complete tree by default!!))
    heap[heapSize] = val;
    printf("original insertion:\n");
    print_heap(heap);

    // now compare this val with parent(s) and swap if necessary, until node
    // settles at correct position
    //
    // note: parents are at (index) / 2, lchild 2*i, rchild 2*i + 1

    // index of value we just inserted
    int curr = heapSize;
    for (int parent = (curr / 2); parent >= 1; parent = (curr / 2)) {
        if (heap[parent] > heap[curr]) {
            printf("parent %d greater than curr child %d\n", heap[parent], heap[curr]);
            swap(&heap[parent], &heap[curr]);
            printf("swap complete: parent now %d\n", heap[parent]);
            curr = parent;
        } else {
            printf("parent %d NOT greater than curr child %d\n", heap[parent], heap[curr]);
            break;
        }
    }
}

// heap sort is basically selection sort + leveraging a data structure to
// quickly find the next largest item on each iteration
void heapSort(int *array, int arraySize) {

    // generate Min-Heap from given values
    int *heap = heapify(array, array + arraySize - 1);
    int *iter = array;

    printf("\n\nResulting heap:\n\n");
    print_heap(heap);

    // print out the smallest values in order
    printf("sorted:\n");
    int min = -1;
    for (int i = 0; i < arraySize; i++) {
        printf("Poppin min...");
        min = popMin(heap);
        printf("\t%d", min);
        printf("\nnew tree:\n");
        print_heap(heap);
        printf("appending to sorted list..");
        *iter++ = min; 
    }
    printf("\n");

    free(heap);
}

void print_heap(int *heap) {
    // height of heap is floor(log2(heapSize) + 1)
    int size = heap[0];
    int height = size ? floor(log2(size) + 1) : 0;
    printf("Heap Size: %d\n", size);
    printf("Heap Height: %d\n", height);

    int counter = 1;
    for (int i = 0; i < height; i++) {
        printf("Level %d:\t", i);
        for (int j = pow(2,i); j < 2*pow(2,i); j++) {
            printf("%d\t", heap[j]);
            if (counter++ == size)
                break;
        }
        printf("\n");
    }
}
