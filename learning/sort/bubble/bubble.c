#include <stdio.h>

#define TRUE 1
#define FALSE 0
#define DEBUG FALSE

void swap(int[], int, int);
void sort(int[], int);
void printArray(int[], int);

int main(void) {
    
    int array[] = { 5, 4, 6, 9, 2, 1, 43, 2 };
    int arraySize = sizeof(array) / sizeof(int);

    printf("unsorted:\n");
    printArray(array, arraySize);
    sort(array, arraySize);
    printf("\nsorted:\n");
    printArray(array, arraySize);

    return 0;
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d\t", array[i]);
    }
    printf("\n");
}

void sort(int array[], int arraySize) {
    if (!arraySize) {
        return;
    }
    int sorted = FALSE;
    while (!sorted) {
        sorted = TRUE;
        for (int i = 0; i < arraySize; i++) {
            if (array[i] > array[i+1]) {
                sorted = FALSE;
                swap(array, i, i+1);
            }
        }
    }
}

void swap(int array[], int i1, int i2) {
    int tmp = array[i1];
    array[i1] = array[i2];
    array[i2] = tmp;
}
