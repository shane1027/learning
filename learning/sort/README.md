# Common Sorting Algos + Pros / Cons

Sorting algorithm implementations in C and Python for the following:

- Bubble Sort
- Selection Sort
- Insertion Sort
- Heap Sort
- Merge Sort
- Quick Sort

*Ordered from least efficient to most efficient*


### 1. [Bubble Sort](https://www.wikiwand.com/en/Bubble_sort)

Compare each item with it's neighbor, and swap if out of order.  Repeat until
no swaps are executed during a pass through the array.

Attributes:

- Slow as hell

- Has 'turtles' and 'rabbits'
    
    - Turtles are small numbers far from their sorted pos in the unsorted array
    - These take a significant number of iterations to bubble to correct pos

    - Rabbits are large numbers far from their sorted pos in the unsorted array
    - These move quickly from the bottom to the top

    - Due to these discrepancies, an algorithm known as [Comb Sort](https://www.wikiwand.com/en/Comb_sort) exists which eliminates turtles by changing the 'comparison gap' is variable, starting large and ending small, such that turtles make large moves early on.



```C
void sort(int array[], int arraySize) {
    int sorted = FALSE;
    while (!sorted) {
        sorted = TRUE;
        for (int i = 0; i < arraySize; i++) {
            if (array[i] > array[i+1]) {
                sorted = FALSE;
                swap(array, i, i+1);
            }
        }
    }
}
```

#### [Optimizations](https://www.wikiwand.com/en/Bubble_sort#/Optimizing_bubble_sor://www.wikiwand.com/en/Bubble_sort#/Optimizing_bubble_sort):

- After the k<sup>th</sup> pass, the k-largest item will have been put into
  place at the top of the list...  therefore, the k<sup>th</sup> pass through
  the list only needs to compare the first n-k items (n = array length).

- More generally, elements after the last swap during a pass through the array
  are already sorted, and can be ignored on future passes.


There's never a good time to use this sorting algorithm...



### 2. [Selection Sort](https://www.wikiwand.com/en/Selection_sort)

Take the smallest value from remaining unsorted portion of the array and append
it to sorted portion of the array.

This is an in-place comparison sort.

O(n<sup>2</sup>) time complexity, inefficient on large lists, performs worse than insertion sort.

Heap Sort is an improvement over selection sort.

```C
void sort(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        int smallest = i;
        for (int j = i; j < arraySize; j++) {
            if (array[j] < array[smallest]) {
                smallest = j;
            }
        }
        swap(array, i, smallest);
    }
}
```

From [Wikipedia](https://www.wikiwand.com/en/Selection_sort)

>Comparison to other sorting algorithms

>Among quadratic sorting algorithms (sorting algorithms with a simple average-case of Θ(n2)), selection sort almost always outperforms bubble sort and gnome sort. Insertion sort is very similar in that after the kth iteration, the first k elements in the array are in sorted order. Insertion sort's advantage is that it only scans as many elements as it needs in order to place the k + 1st element, while selection sort must scan all remaining elements to find the k + 1st element.

>Simple calculation shows that insertion sort will therefore usually perform about half as many comparisons as selection sort, although it can perform just as many or far fewer depending on the order the array was in prior to sorting. It can be seen as an advantage for some real-time applications that selection sort will perform identically regardless of the order of the array, while insertion sort's running time can vary considerably. However, this is more often an advantage for insertion sort in that it runs much more efficiently if the array is already sorted or "close to sorted."

>While selection sort is preferable to insertion sort in terms of number of writes (Θ(n) swaps versus Ο(n2) swaps), it almost always far exceeds (and never beats) the number of writes that cycle sort makes, as cycle sort is theoretically optimal in the number of writes. This can be important if writes are significantly more expensive than reads, such as with EEPROM or Flash memory, where every write lessens the lifespan of the memory.

>Finally, selection sort is greatly outperformed on larger arrays by Θ(n log n) divide-and-conquer algorithms such as mergesort. However, insertion sort or selection sort are both typically faster for small arrays (i.e. fewer than 10–20 elements). A useful optimization in practice for the recursive algorithms is to switch to insertion sort or selection sort for "small enough" sublists. 


### 3. [Insertion Sort](https://www.wikiwand.com/en/Insertion_sort)

For each item in the array, find the correct position in the sorted sub-array to
append the value.

Sorts in place, performs better than selection and bubble sort on average,
although uses twice as many writes as selection sort on average (since we are
shifting the entire sorted sub-array upon insertion).  The more ordered an
array upon input, the closer the algorithmic run-time is to linear.

Adaptive: O(kn) when each item is no more than k positions away from sorted
position.

Also, insertion sort is an [online
algorithm](https://www.wikiwand.com/en/Online_algorithm), meaning it can
process information as it arrives in a stream.

(Another example of an online algorithm is [reservoir
sampling](https://www.wikiwand.com/en/Reservoir_sampling))

```C
void sort(int array[], int arraySize) {
    for (int i = 1; i < arraySize; i++) {
        int dest = 0;
        for (int j = i - 1; j >= 0; j--) {
            if (array[i] > array[j]) {
                dest = j + 1;
                break;
            }
        }
        insert(array, i, dest);
    }
}
```

#### [Optimizations]():

- Shell Sort compares items separated by some interval and swaps them if
  necessary... this interval is then decreased, and finally Insertion sort is
  executed.



### 4. [Heap Sort](https://www.wikiwand.com/en/Heapsort)

An improvement over selection sort, leveraging the heap data structure to make
comparisons.

>Heapsort can be thought of as an improved selection sort: like that algorithm,
>it divides its input into a sorted and an unsorted region, and it iteratively
>shrinks the unsorted region by extracting the largest element and moving that
>to the sorted region. The improvement consists of the use of a heap data
>structure rather than a linear-time search to find the maximum.

In-place, very useful for medium to large sized datasets on systems with
limited memory.


### 5. [Merge Sort](https://www.wikiwand.com/en/Mergesort)

Recursively split the array in consideration in half, and sort each sub-array.
Recombine or merge these two sub-arrays into a single sorted one.

Difficult to implement in-place.  Therefore, has a higher space requirement
than all of the above - O(n).


```C
int *sort(int *start, int *end) {
    if (start == end) {
        return start;
    }

    int size = end - start + 1;
    int half = (end - start) / 2;

    int *left = sort(start, start+half);
    int *right = sort(start+half+1, end);

    // allocate temporary space for storing the sorted array
    int *sorted = malloc(size * sizeof(int));
    int *iter = sorted;

    while (left <= start+half && right <= end) {
        if (*left <= *right) {
            *iter++ = *left++;
        } else {
            *iter++ = *right++;
        }
    }

    while (left <= start+half) {
        *iter++ = *left++;
    }

    while (right <= end) {
        *iter++ = *right++;
    }


    // copy sorted array back into the original datastructure and free mem
    memcpy(start, sorted, size * sizeof(int));
    free(sorted);

    return start;
}
```

> Comparison with other sort algorithms

> Although heapsort has the same time bounds as merge sort, it requires only Θ(1) auxiliary space instead of merge sort's Θ(n). On typical modern architectures, efficient quicksort implementations generally outperform mergesort for sorting RAM-based arrays.[citation needed] On the other hand, merge sort is a stable sort and is more efficient at handling slow-to-access sequential media. Merge sort is often the best choice for sorting a linked list: in this situation it is relatively easy to implement a merge sort in such a way that it requires only Θ(1) extra space, and the slow random-access performance of a linked list makes some other algorithms (such as quicksort) perform poorly, and others (such as heapsort) completely impossible. 

Summary: Heapsort is equal speed with constant memory footprint vs. merge
sort's 0(n) memory footprint.  However, merge sort is stable, (maintains the
order of items with identical values), and more efficient at handling
slow-to-access meda such as a disk or a linked-list.  In fact, quicksort
performs slowly on linked-lists, and heapsort is impossible since there is no
random lookup, so merge sort is *great* for linked-list sorting... otherwise
you probably have better options (quick and heap).


### 6. [Quick Sort](https://www.wikiwand.com/en/Quicksort)

Divide-and-conquer algorithm that chooses 'pivot' values iteratively, and
partitions array into sections less than and greater than the pivot.

Interesting reading on practical outcome vs heapsort
[here](https://www.cs.auckland.ac.nz/software/AlgAnim/qsort3.html).


```C
void sort(int *start, int *end) {
    if (start >= end) {
        return;
    }
    int *offset = partition(start, end);
    sort(start, offset-1);
    sort(offset+1, end);
}

int *partition(int *start, int *end) {
    int *slow = start;
    int *fast = start;

    while (fast < end) {
        if (*fast <= *end) {
            swap(slow++, fast);
        }
        fast++;
    }

    swap(slow, end);
    return slow;
}

void swap(int *dest, int *src) {
    int tmp = *dest;
    *dest = *src;
    *src = tmp;
}
```


### 7. [Binary Tree Sort](https://www.wikiwand.com/en/Tree_sort)

Build a binary search tree from the data and iterate through in-order to sort
information.  

This algorithm is online, meaning it can deal with streams of data dynamically.

Adding an item to a binary tree is proportional to the height h, which in turn
is related to the number of items n via floor(log<sub>2</sub>n).  Thus, adding
one item on average takes O(log<sub>2</sub>n), and adding n items takes O(nlog<sub>2</sub>n).

Unbalanced trees take O(n) worst case for adding / modifying an item.  Thus, it
is necessary to keep the input data randomized, or perform 'repair' steps on
the tree after each insertion / deletion via rotations.  This approach
leverages self-balancing binary trees.

Using self-balancing binary trees creates a comparison sort running at the
ideal O(nlog<sub>2</sub>n) time worst-case, and average-case, but requires a
significant amount of memory to be allocated on the heap compared to quicksort
and heapsort.





Table of Runtimes:

<table>
    <tr>
        <td>Algorithm</td>
        <td>Average Case</td>
        <td>Best Case</td>
        <td>Worst Case</td>
    </tr>
    <tr>
        <td>Bubble Sort</td>
        <td>hmm</td>
        <td>idk</td>
        <td>always bad</td>
    </tr>
    <tr>
        <td>Selection Sort</td>
        <td>this</td>
        <td>is</td>
        <td>selection</td>
    </tr>
    <tr>
        <td>Insertion Sort</td>
        <td>hello</td>
        <td>insertion</td>
        <td>sort</td>
    </tr>
    <tr>
        <td>Heap Sort</td>
        <td>you get the idea</td>
        <td>hopefully</td>
        <td>by now</td>
    </tr>
    <tr>
        <td>Quick Sort</td>
        <td>you get the idea</td>
        <td>hopefully</td>
        <td>by now</td>
    </tr>
    <tr>
        <td>Merge Sort</td>
        <td>you get the idea</td>
        <td>hopefully</td>
        <td>by now</td>
    </tr>
</table>
