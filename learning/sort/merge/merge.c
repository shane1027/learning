#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define DEBUG TRUE

int *sort(int*, int*);
void printArray(int[], int);

int main(void) {
    
    int array[] = { 5, 4, 6, 9, 2, 1, 43, 2 };
    int arraySize = sizeof(array) / sizeof(int);

    printf("unsorted:\n");
    printArray(array, arraySize);
    int *sorted = sort(array, array + arraySize - 1);
    printf("\nsorted:\n");
    printArray(sorted, arraySize);

    return 0;
}

void printArray(int array[], int arraySize) {
    for (int i = 0; i < arraySize; i++) {
        printf("%d\t", array[i]);
    }
    printf("\n");
}

int *sort(int *start, int *end) {
    if (start == end) {
        if (DEBUG)
            printf("returning single item %d\n", *start);
        return start;
    }

    int size = end - start + 1;
    int half = (end - start) / 2;


    if (DEBUG) {
        printf("Size: %d\tIndex for Half: %d\n", size, half);
        printf("Splitting into: \n");
        printArray(start, half+1);
        printArray(start+half+1, half+1);
    }

    int *left = sort(start, start+half);
    int *right = sort(start+half+1, end);

    // allocate temporary space for storing the sorted array
    int counter = 0;
    int *sorted = malloc(size * sizeof(int));
    int *iter = sorted;

    while (left <= start+half && right <= end) {
        if (*left <= *right) {
            *iter++ = *left++;
            counter++;
            if (DEBUG) {
                printf("%d from left is less than %d from right\n", *(left-1), *right);
                printf("Sorted:  ");
                printArray(sorted, counter);
            }
        } else {
            *iter++ = *right++;
            counter++;
            if (DEBUG) {
                printf("%d from right is less than %d from left\n", *(right-1), *left);
                printf("Sorted:  ");
                printArray(sorted, counter);
            }
        }
    }

    while (left <= start+half) {
        *iter++ = *left++;
        counter++;
        if (DEBUG) {
            printf("appending remaining left side: %d\n", *(left-1));
            printf("Sorted:  ");
            printArray(sorted, counter);
        }
    }

    while (right <= end) {
        *iter++ = *right++;
        counter++;
        if (DEBUG) {
            printf("appending remaining right side: %d\n", *(right-1));
            printf("Sorted:  ");
            printArray(sorted, counter);
        }
    }


    // copy sorted array back into the original datastructure and free mem
    memcpy(start, sorted, size * sizeof(int));
    free(sorted);

    return start;
}
