#include <stdio.h>

char mainstring[] = "abaabcababacabc";
char substring[] = "aba";

int main(void) {

    char *mainiter = mainstring;
    char *chunkiter = mainiter;
    char *subiter = substring;
    int subcount = 0;
    int insub = 0;
    
    while (*mainiter) {
        chunkiter = mainiter;
        while (*subiter) {
            if (*chunkiter == *subiter) {
                insub = 1;
            } else {
                insub = 0;
                break;
            }
            subiter++;
            chunkiter++;
            if (!*chunkiter) break;
        }
        if (insub) subcount++;
        mainiter++;
        subiter = substring;
        insub = 0;
    }

    printf("subcount: %d\n", subcount);
    return 0;
}
